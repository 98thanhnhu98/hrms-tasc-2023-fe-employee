// import { DatePicker } from 'antd';
import './App.css';
// import { createRoot } from "react-dom/client";
// import { Button, Space, version } from "antd";
import 'antd/dist/reset.css';
import { Routes, Route } from "react-router-dom";
import { routes } from './config/router/routes';
import { Fragment } from 'react';

function App() {
  return (
    <Routes>
      {routes.map((item, index) => {
        const Page = item.component;
        let Layout = null;

        if (item.changeLayout) {
          Layout = item.changeLayout;
        } else {
          Layout = Fragment;
        }
        return (
          <Route key={index} path={item.path}
            element={
              <Layout>
                <Page />
              </Layout>
            }
          />
        )
      })}
    </Routes>
  );
}

export default App;

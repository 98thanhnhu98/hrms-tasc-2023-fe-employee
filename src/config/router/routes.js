import Login from "../../containers/layout/Authenticate/Login";
import Dashboard from "../../containers/layout/Dashboard/Dashboard";
import Profile from "../../containers/layout/Profile/Profile";
import test from "../../containers/layout/Timekeeping/TimeKeeping";
import TimeKeeping from "../../containers/layout/Timekeeping/TimeKeeping";

const routes = [
    { path: '/dashboard/profile' , component: Profile , changeLayout: Dashboard},
    { path: '/dashboard/timekeeping' , component:TimeKeeping , changeLayout:Dashboard},
    { path: '/login',component:Login},
    { path: '/test',component:test},

];

export {routes};
import './Profile.css'
import React from 'react';
const user = {
    fullname: localStorage.getItem("fullname"),
    gender: localStorage.getItem("gender"),
    birthday: localStorage.getItem("dob"),
    identityNumber: localStorage.getItem("identityNumber"),
    inPlace: localStorage.getItem("inPlace"),
    inDate: localStorage.getItem("inDate"),
    phone: localStorage.getItem("phone"),
    email: localStorage.getItem("email"),
    address: localStorage.getItem("address"),
    imageId: localStorage.getItem("imageId"),

    status: localStorage.getItem("status"),
    position: localStorage.getItem("positionId"),
    recruitedDate: localStorage.getItem("recruitedDate"),
    quitDate: localStorage.getItem("quitDate"),
    quitReason: localStorage.getItem("quitReason"),
    isDeleted: localStorage.getItem("isDeleted"),
    note: localStorage.getItem("note"),



}

function Profile() {
    return (<>
        <h1>Thông tin nhân viên</h1>
        <div className='profile'>
            <div className="image-profile">
                <div className="avatar">
                    <img className='avt-item'  src={"../../../asset/image/no-avt.jpg"} alt=""  />
                    {/* <img src={user.imageId} alt={user.imageId} /> */}
                </div>
            </div>
            <div className='main-profile'>
                <div className="main-profile-item">
                    <span className='title-item'>Họ Tên: </span>
                    <span>{user.fullname}</span>
                </div>
                <div className="main-profile-item">
                    <span className='title-item'>Giới tính: </span>
                    <span>{user.gender}</span>
                </div>
                <div className="main-profile-item">
                    <span className='title-item'>Ngày sinh: </span>
                    <span>{user.birthday}</span>
                </div>
                <div className="main-profile-item">
                    <span className='title-item'>Địa chỉ: </span>
                    <span>{user.address}</span>
                </div>
                <div className="main-profile-item">
                    <span className='title-item'>CCCD/CMND: </span>
                    <span>{user.identityNumber}</span>
                </div>
                <div className="main-profile-item">
                    <span className='title-item'>Nơi cấp: </span>
                    <span>{user.inPlace}</span>
                </div>
                <div className="main-profile-item">
                    <span className='title-item'>Ngày cấp: </span>
                    <span>{user.inDate}</span>
                </div>
                <div className="main-profile-item">
                    <span className='title-item'>Tên ngân hàng: </span>
                    <span>{user.bankName}</span>
                </div>
                <div className="main-profile-item">
                    <span className='title-item'>CCCD/CMND: </span>
                    <span>{user.identityNumber}</span>
                </div>
                <div className="main-profile-item">
                    <span className='title-item'>Số điện thoại: </span>
                    <span>{user.phone}</span>
                </div>
                <div className="main-profile-item">
                    <span className='title-item'>Email: </span>
                    <span>{user.email}</span>
                </div>
            </div>
            <div className="profile-company">
                <div className="profile-company-item">
                    <span>Trạng thái làm việc:</span>
                    <span>{user.status}</span>
                </div>
                <div className="profile-company-item">
                    <span>Chức vụ:</span>
                    <span>{user.position}</span>
                </div>
                <div className="profile-company-item">
                    <span>Ngày vào làm:</span>
                    <span>{user.recruitedDate}</span>
                </div>
                <div className="profile-company-item">
                    <span>Ngày nghỉ việc:</span>
                    <span>{user.quitDate}</span>
                </div>
                <div className="profile-company-item">
                    <span>Lý do nghỉ việc:</span>
                    <span>{user.quitReason}</span>
                </div>
                <div className="profile-company-item">
                    <span>Trạng thái trạng thái nghỉ việc:</span>
                    <span>{user.isDeleted}</span>
                </div>
                <div className="profile-company-item">
                    <span>Ghi chú:</span>
                    <span>{user.note}</span>
                </div>
            </div>
        </div>
        <button>Sửa</button>
    </>
    );
}

export default Profile;
import { useEffect } from "react";
import './css/Login.css'
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const Login = () => {
    const [profile, setProfile] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState(false);

    useEffect(() => {
        saveUserDataInLocalStorageFunc();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [profile]);

    // api
    const LOGIN_API = `http://localhost:8080/api/v1/auth/authenticate`;
    const GET_USER_DATA_API = `http://localhost:8080/api/v1/userDetail/currentUser`;

    // config
    const axiosConfig = {
        headers: {
            "Content-Type": "application/json",
            accept: "application/json",
        },
    };

    const token = localStorage.getItem("accessToken");
    const axiosGetUserDataConfig = {
        headers: {
            "Content-Type": "application/json",
            accept: "application/json",
            Authorization: `Bearer ${token}`,
        },
        
            
    };
    

    // form
    const account = JSON.stringify({
        username: username,
        password: password,
    });

    const navigate = useNavigate();

    const loginHandleFunction = async () => {
        try {
            const response = await axios.post(LOGIN_API, account, axiosConfig);
            localStorage.setItem("accessToken", response.data.accessToken);
            localStorage.setItem("refreshToken", response.data.refreshToken);
            setProfile(response.data.accessToken);
            console.log(response);
        } catch (error) {
            setError("Tài khoản hoặc mật khẩu sai, vui lòng đăng nhập lại");
            console.log(error);
        }
    };

    const saveUserDataInLocalStorageFunc = async () => {
        try {
            const response = await axios.get(
                GET_USER_DATA_API,
                axiosGetUserDataConfig
            );
            if(response.status === 401&& response.data.name === "EXPIRED_ACCESS_TOKEN"){
                navigate("/login");
            }
            console.log(response);
            localStorage.setItem("fullname", response.data.data.fullName);
            localStorage.setItem("identityNumber", response.data.data.identityNumber);
            localStorage.setItem("bankNumber", response.data.data.bankNumber);
            localStorage.setItem("phone", response.data.data.phone);
            localStorage.setItem("address", response.data.data.address);
            localStorage.setItem("status", response.data.data.status);
            localStorage.setItem("isDeleted", response.data.data.isDeleted);
            localStorage.setItem("dob", response.data.data.dob);
            if(response.data.data.genderId===3){
                localStorage.setItem("gender", "Nam");
            }else if(response.data.data.genderId===3){
                localStorage.setItem("gender", "Nữ");
            }else{
                localStorage.setItem("gender", "Khác");
            }
            localStorage.setItem("positionId", response.data.data.positionId);
            localStorage.setItem("bankId", response.data.data.bankId);
            localStorage.setItem("imageId", response.data.data.imageId);
            localStorage.setItem("bankNumber", response.data.data.bankNumber);
            localStorage.setItem("recruitedDate", response.data.data.recruitedDate);
            localStorage.setItem("email", response.data.data.email);
            localStorage.setItem("inDate", response.data.data.inDate);
            localStorage.setItem("inPlace", response.data.data.inPlace);
            localStorage.setItem("quitDate", response.data.data.quitDate);
            localStorage.setItem("quitReason", response.data.data.quitReason);
            localStorage.setItem("note", response.data.data.note);
            navigate("/dashboard/timekeeping");
            window.location.reload(false);
        } catch (error) {
            console.log(error);
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        await loginHandleFunction();
    };
    return (
        <div className="wrapper">
            <form autoComplete="on" className="form" onSubmit={handleSubmit}>
                <div className="control">
                    <h1>Đăng nhập</h1>
                </div>
                <div className="control block-cube block-input">
                    <label >Tài khoản: </label>
                    <input
                        type="text"
                        id="username"
                        name="username"
                        autoComplete="on"
                        checked
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        placeholder="Tên người dùng"
                    />
                </div>
                <div className="control block-cube block-input">
                    <label>Mật khẩu: </label>
                    <input
                        type="password"
                        id="password"
                        name="password"
                        checked
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        placeholder="Mật khẩu"
                    />
                </div>
                <div style={{ marginBlock: "2rem" }}>
                    {error && <span className="form-failed">{error}</span>}
                </div>
                <button className="btn block-cube block-cube-hover" type="submit">
                            <div className="text">Đăng nhập</div>
                </button>
            </form>
        </div>
    );
};

export default Login;
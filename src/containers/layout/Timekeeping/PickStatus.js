
import { Component } from "react";
import formatDate from "../../../ultils/FormatDate";
import './PickStatus.css'

class PickStatus extends Component {
    constructor() {
        super();
        this.state = {
            name: "React",
            selectedOption: "Không chọn"
        };

        this.onValueChange = this.onValueChange.bind(this);
        this.formSubmit = this.formSubmit.bind(this);
    }

    onValueChange(event) {
        const date = new Date()
        this.setState({
            selectedOption: event.target.value,
            Dateoff: formatDate(date)
        });
    }

    formSubmit(event) {
        event.preventDefault();
        console.log(this.state)
    }

    render() {
        return (
            <form className="container" onSubmit={this.formSubmit}>
                <div className="list-item">
                    {/* <div className="radio">
                        <div className="radio-item">
                            <input id="NotOff"
                                type="radio"
                                value="Không Nghỉ"
                                checked={this.state.selectedOption === "Không Nghỉ"}
                                onChange={this.onValueChange}
                            />
                            <label htmlFor="NotOff" className="span-text"> Không Nghỉ</label>
                        </div>
                    </div> */}
                    <div className="radio">
                        <div className="radio-item">
                            <input id="morningbreak"
                                type="radio"
                                value="Nghỉ Sáng"
                                checked={this.state.selectedOption === "Nghỉ Sáng"}
                                onChange={this.onValueChange}
                            />
                            <label htmlFor="morningbreak" className="span-text"> Nghỉ Sáng</label>
                            
                        </div>
                    </div>
                    <div className="radio">
                        <div className="radio-item">
                            <input id="afterbreak"
                                type="radio"
                                value="Nghỉ Chiều"
                                checked={this.state.selectedOption === "Nghỉ Chiều"}
                                onChange={this.onValueChange}
                            />
                            <label htmlFor="afterbreak" className="span-text"> Nghỉ Chiều</label>
                            
                        </div>
                    </div>
                    <div className="radio">
                        <div className="radio-item">
                            <input id="dateoff"
                                type="radio"
                                value="Nghỉ cả ngày"
                                checked={this.state.selectedOption === "Nghỉ cả ngày"}
                                onChange={this.onValueChange}
                            />
                            <label htmlFor="dateoff" className="span-text">  Nghỉ cả ngày</label>
                           
                        </div>
                    </div>
                </div>

                <div className="selected-input">
                    Bạn đang chọn: {this.state.selectedOption}
                </div>
                <div className="btn-container">
                    <button className="btn-submit btn btn-default" type="submit">
                        Submit
                    </button>
                </div>
            </form>
        );
    }
}

export default PickStatus;

import { disableCursor } from '@fullcalendar/core/internal';
import daygrid from '@fullcalendar/daygrid';
import interaction from '@fullcalendar/interaction';
import FullCalendar from '@fullcalendar/react';
import React from 'react'
import formatDate from '../../../ultils/FormatDate';
import Modal from './Modal';
import useModal from './userModal';

// eslint-disable-next-line import/no-anonymous-default-export
function TimeKeeping() {


    const { isShowing, toggle } = useModal();
    const date=formatDate(new Date());
    return (
        <div>

            <FullCalendar plugins={[daygrid, interaction]}
                dateClick={(e) => {
                    const dateToday= formatDate(new Date(e.dateStr));
                    if(dateToday === date){
                        console.log(e.dateStr);
                        localStorage.setItem("today", formatDate(new Date(e.dateStr)));
                        toggle();
                    }else{
                        console.log(e.dateStr);
                        console.log(date);
                    }
                }}
                
                height={700}

                initialView="dayGridMonth"

                events={
                    { title: 'Meeting', start: new Date().toLocaleDateString("en-US") }
                }

                eventContent={RenderContent}

            />
            <Modal
                isShowing={isShowing}
                hide={toggle}
            />
            {/* <button className="button-default" onClick={toggle}>Show Modal</button> */}

        </div>
    )

}
function RenderContent(eventInfo) {
    <>
        <b>{eventInfo.timeText}</b>
        <i>{eventInfo.event.title}</i>
    </>
}
export default TimeKeeping

import React from 'react';
import ReactDOM from 'react-dom';
import Timekeeping from './PickStatus';
import './Modal.css'

const Modal = ({ isShowing, hide }) => isShowing ? ReactDOM.createPortal(
    <React.Fragment>
        <div className='root-modal'>
            <div className="modal-overlay" />
            <div className="modal-wrapper"  role="dialog">
                <div className="modal">
                    <div className="modal-header">
                        <button type="button" className="modal-close-button" data-dismiss="modal"  onClick={hide}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <Timekeeping />
                </div>
            </div>
        </div>
    </React.Fragment>, document.body
) : null;

export default Modal;
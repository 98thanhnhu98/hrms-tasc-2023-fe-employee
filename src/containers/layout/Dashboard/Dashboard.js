import {
    // AppstoreOutlined,
    // BarChartOutlined,
    // CloudOutlined,
    // ShopOutlined,
    // TeamOutlined,
    // UploadOutlined,
    UserOutlined,
    // VideoCameraOutlined,
    // PushpinOutlined,
    LogoutOutlined,
    ReconciliationOutlined,
    MenuUnfoldOutlined,
    MenuFoldOutlined
} from '@ant-design/icons';
import { Layout, Menu, theme } from 'antd';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './style.css';
// import pageRoutes from '../../../config/router';

const { Header, Content, Sider } = Layout;

function Dashboard({ children }) {
    const [collapsed, setCollapsed] = useState(true);
    const navigate = useNavigate();

    const {
        token: { colorBgContainer },
    } = theme.useToken();
    return (
        <Layout hasSider>
            <Sider style={{ overflow: 'auto', height: '100vh', left: 0, top: 0, bottom: 0, }}
                trigger={null} collapsible collapsed={collapsed}>
                <div style={{ height: 32, margin: 16, background: 'rgba(255, 255, 255, 0.2)', }} />
                <div style={{ height: 32, margin: 16, background: 'rgba(255, 255, 255, 0.2)', }} />
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}
                    items={[
                        {
                            key: '1',
                            icon: <UserOutlined />,
                            label: 'Thông Tin',
                            onClick: ()=>{
                                navigate("/dashboard/profile")
                            }
                        },
                        {
                            key: '2',
                            icon: <ReconciliationOutlined />,
                            label: 'Chấm công',
                            onClick: ()=>{
                                navigate("/dashboard/timekeeping")
                            }
                            
                        }
                        ,
                        {
                            key: '3',
                            icon: <LogoutOutlined />,
                            label: 'Chấm công',
                            onClick:()=>{
                                localStorage.clear();
                                navigate("/login");
                                window.location.reload(false)
                            }
                            
                        }
                    ]} />
            </Sider>
            <Layout
                className="site-layout">
                <Header style={{ padding: 0, background: colorBgContainer }}>
                    {/* <Icone className="trigger" type={collapsed ? 'menu-unford' : 'menu-fold'} onClick={() => setCollapsed(!collapsed)} /> */}
                    {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                        className: 'trigger',
                        onClick: () => setCollapsed(!collapsed),
                    })}
                </Header>
                <Content style={{ margin: '24px 16px 0' }}>
                    <div style={{ padding: 24, background: colorBgContainer }}>
                        {children}
                    </div>
                </Content>
            </Layout>
        </Layout>
    );
};
export default Dashboard;